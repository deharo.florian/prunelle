<?php session_start() ;
extract($_GET);?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>

<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="divform">
                <form action="ajouterEmpochage.php" method="POST">
                    <p class="h1 text-center">Ajout d'empochage pour la commande n°<?= $id?></p>
                    <div class="form-group">
                    <select class="form-select" required name="empocheur" aria-label="Default select example">
                                <option selected disabled>choisissez l'empocheur</option>
                        <?php

                        require "C:/xampp/htdocs/Prunelle_florian/config.php";
                        $bdd = connect();


                        $sql="select * from utilisateur" ; 
                        //execution de la requete
                        $resultat=$bdd->query($sql); ?><?php
                        while ($empocheur = $resultat->fetch(PDO::FETCH_OBJ)) {
                             var_dump($resultat); ?>

                                <option value="<?= $empocheur->Id_utilisateur ?>"><?= $empocheur->prenom  ?></option>
                                
                        <?php }
                        
                        ?>        
                            </select>
                    </div>

                    <div class="form-group">
                            <label for="debut">entrez le debut de l'empochage</label>
                            <input type="datetime-local" required  class="form-control" name="debut" min="2017-06-01T08:30" max="<?= date('Y-m-d h:i:s') ?>" id="debut">
                    </div>
                    <div class="form-group">
                            <label for="fin">entrez la fin de l'empochage</label>
                            <input type="datetime-local" required class="form-control" name="fin" min="2017-06-01T08:30" max="<?= date('Y-m-d h:i:s') ?>" id="fin">
                    </div>
                    <input type="hidden" name="idcommande" value="<?= $id?>">
                    <input type="submit" class="btn btn-success  my-3" value="ajouter">

                </form>
            </div>
        </div>
    </div>
</div>

<?php }else{ ?>
    <p>page interdite</p>
<a href="accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } 


 include("C:/xampp/htdocs/Prunelle_florian/fragment/footer.php") ; ?>      
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

</html>