<?php session_start() ;
extract($_GET);?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nouvelle commande</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>

<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="divform">
                <form action="creation2.php" method="POST">
                    <p class="h1 text-center">Creation commande </p>
                    <div class="form-group">
                        <label for="">entrez le numero de commande</label>                         
   
                            <input class="form-control" type="number" min="1" name="commande" id="">
                   
                    </div>
                    <div class="form-group">
                        <label for="">entrez le numero de la prise de vue</label>                         
   
                            <input class="form-control"  type="number" min="1" name="pdv" id="">
                   
                    </div>

                    <input type="submit" class="btn btn-success  my-3" value="ajouter">

                </form>
            </div>
        </div>
    </div>
</div>

<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/footer.php") ;?>
 <?php } ?>      
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

</html>