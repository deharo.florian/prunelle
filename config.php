<?php


define("HOTE", 'localhost');
define("BDD", 'prunelle');
define("UTILISATEUR", 'root');
define("MDP", '');

function connect()
{

    try {
        $connect = new PDO('mysql:host='.HOTE.';dbname='.BDD,UTILISATEUR,MDP);
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION) ; 
    } catch (PDOException $e) {
        echo "probleme de connexion à la BDD <br>" . $e->getMessage();
    }
    return $connect;
};

function dateEtHeureFR($date){
    $date = explode(' ', $date, 2);
    $dateh = $date[1];
    $date = explode('-', $date[0], 3);
     $datefr = $dateh."h ".$date[2]."/".$date[1]."/".$date[0] ;
     return $datefr;
   }

function dateHeureFR($date){
    $date = explode(' ', $date, 2);
    $date = explode('-', $date[0], 3);
     $datefr = $date[2]."/".$date[1]."/".$date[0] ;
     return $datefr;
   }

function dateFR($date){
    $date = explode('-', $date, 3);
     $datefr = $date[2]."/".$date[1]."/".$date[0] ;
     return $datefr;
   }
