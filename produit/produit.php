<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>

<body>
    <?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; ?>
<div class="container">
   
    <div class="row text-center py-5">
        <div class="col  alert-warning"><h2>Nos produits</h2></div>
        <div class="row my-4">
             <div class="col col-4 offset-4">
          <p class="text-center" style="border:3px solid yellow">
        <?php
      if(isset($_SESSION["action"])){ ?>
        <p class="text-center session alert-warning" >
        <?= $_SESSION["action"]; ?>   </p>  
    <?php  }  

 
 if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
        <a href='formajout.php'  class='btn btn-outline-success' >ajouter</a>
   

     <?php } ?>
    </p>  
    <div>
    <form class="d-flex" action="recherche.php" method="POST">
        <input class="form-control me-2" type="search" name="barre" placeholder="recherche" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">recherche</button>
        
      </form>
    </div>
        </div>    
        </div>
    
    </div>
    <div class="row d-flex justify-content-center justify-content-sm-start">
        <?php
  
    require "C:/xampp/htdocs/Prunelle_florian/config.php";
    $bdd = connect();
    
   
    $sql="select * from produit" ; 
//execution de la requete
$resultat=$bdd->query($sql);
         while ($produit = $resultat->fetch(PDO::FETCH_OBJ)) { ?>
           
    
    <div class='card m-1' style='width: 15rem;'>
        <div class='card-body'>
      <h5 class='card-title'><?= $produit->nom ."<br> ". $produit->prix ?>€</h5>
     
     <?php if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
        <a href='modifier.php?id=<?= $produit->Id_produit ?>' class='btn btn-outline-success' >modifier</a>
     <a href='supprimer.php?id=<?= $produit->Id_produit ?>' class='btn btn-outline-success'>supprimer</a>

     <?php } ?>
     
      <p></p>
    </div>
  </div>
  <?php
        }

      
        ?>
    </div>
</div>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/footer.php") ; ?>
</body>
<script src="/prunelle_florian/script/script.js"></script>

</html>