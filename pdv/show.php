<?php 
session_start() ; 
extract($_GET); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>prise de vue</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>
<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION["autorisation"]) and $_SESSION["autorisation"]=="ok"){ ?>
<div class="container">
    <div class="row">
        <div class="col offset-2 col-8">
           
                <?php
  
                    require "C:/xampp/htdocs/Prunelle_florian/config.php";
                    $bdd = connect();
                    
                    
                    $sql="select * from prise_de_vue where Id_prise_de_vue =".$id ; 
                    //execution de la requete
                    $resultat=$bdd->query($sql);
                        while ($pdv = $resultat->fetch(PDO::FETCH_OBJ)) { ?>
                        <div class="col col-6">

                            <div class="card m-2"  >
                                <div class="card-body">
                                    <h5 class="card-title">Prise de vue n<?= $pdv->Id_prise_de_vue ?></h5>
                                    <h6 class="card-title mb-2">ecole n<?= $pdv->id_ecole ?></h6>
                                    <p class="card-text">date et heure : <?= $pdv->date_pdv?></p>
                                    <p class="card-text">frequence : <?= $pdv->frequence?></p>
                                    <p class="card-text">type de prise de vue : <?= $pdv->type_pdv ?></p>
                                    <p class="card-text">theme : <?= $pdv->theme ?></p>
                                    <p class="card-text">vente : <?= $pdv->type_vente ?></p>
                                    <p class="card-text">nombre d'eleves : <?= $pdv->nombre_eleve ?></p>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
             
          
        </div>
    </div>
</div>

<?php
}
else { ?>
<p>page interdite</p>
<a href="/prunelle_florian/accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } ?>


</body>
</html>