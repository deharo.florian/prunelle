<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>

<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ;
if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){  ?>
<div class="container">
  <div class="row">
    <div class="col col-6 offset-3">
      <div class="divform">

        <form action="ajouter.php" method="POST">
          <h1 class="text-center my-5">Formulaire d'ajout de prises de vue</h1>
          <div class="form-group">
            <label for="numero">entrez le numero de la prise de vue</label>
            <input type="number" name="Id_prise_de_vue" class="form-control" min="0" id="numero" placeholder="numero prise de vue">
          </div>
          <div class="form-group">
          <label for="numero">entrez le numero de l'ecole</label>
          <input type="number" name="id_ecole" class="form-control"  min="0" id="numeroEcole" placeholder="numero Ecole">
        </div>     
        <div class="form-group">
          <label for="date_pdv">entrez la date</label>
          <input type="date" name="date_pdv" class="form-control" id="date_pdv" placeholder="date">
        </div>    
        <div class="form-group">
          <label for="nom">entrez la durée (en minute)</label>
          <input type="number" name="duree" class="form-control" min="0" id="duree" placeholder="durée">
        </div>    
        <div class="form-group">
          <label for="nom">entrez la frequence</label>
          <select class="form-select" name="frequence" aria-label="Default select example">
            <option selected disabled>choisissez la frequence</option>
            <option value="D1">D1</option>
            <option value="D2">D2</option>
            <option value="D1/D1">D1/D1</option>
            <option value="D1/D2">D1/D2</option>
            <option value="D2/D1">D2/D1</option>
            <option value="D2/D2">D2/D2</option>
          </select>
        </div> 
        <div class="form-group">
          <label for="nom">entrez le type de prise de vue</label>
          <select class="form-select"  name="type_pdv" aria-label="Default select example">
            <option selected disabled>type prise de vue</option>
            <option value="individuelle">individuelle</option>
            <option value="groupe">groupe</option>
            <option value="individuelle et groupe">individuelle + groupe</option>
          </select>        
        </div>  
        
        <div class="form-group">
          <label for="nom">entrez le theme</label>
          <select class="form-select" name="theme"  aria-label="Default select example">
            <option selected disabled>theme</option>
            <option value="ecolo">ecolo</option>
            <option value="table">table</option>
            <option value="mains">mains</option>
            <option value="stand">stand</option>
            
          </select>        
        </div>    
        <div class="form-group">
          <label for="nom">entrez la vente</label>
          <select class="form-select"  name="type_vente"  aria-label="Default select example">
            <option selected disabled>vente</option>
            <option value="vente direct">vente direct</option>
            <option value="Bon de commande">Bon de commande</option>
            <option value="internet">internet</option>
            <option value="semi-direct">semi-direct</option>
          </select>        
        </div> 
        <div class="form-group">
          <label for="nombre_eleve">entrez le nombre d'eleve</label>
          <input type="number" name="nombre_eleve" class="form-control"  min="0" id="nombre_eleve" placeholder="nombre Eleve ">
        </div>   
        <input type="submit" class="btn btn-success  my-3" value="ajouter">
      </form>
    </div>
    </div>
  </div>
          </div>
          <?php include("C:/xampp/htdocs/Prunelle_florian/fragment/footer.php") ;
}else{ ?>
    <p>page interdite</p>
<a href="accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } ?>
</body>
 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

</html>