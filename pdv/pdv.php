<?php 
session_start() ; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>
<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION["autorisation"]) and $_SESSION["autorisation"]=="ok"){ ?>
<div class="container">
<div class="row text-center py-5">
        <div class="col alert-warning"><h2>Les prises de vue</h2></div>
        <div class="row my-4">
             <div class="col col-4 offset-4">
        <?php
     if(isset($_SESSION["action"])){ ?>
        <p class="text-center session alert-warning" >
        <?= $_SESSION["action"]; ?>   </p>  
    <?php  }  

 
 if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
        <a href='/prunelle_florian/pdv/formajout.php'  class='btn btn-outline-success' >ajouter</a>
   

     <?php } ?>

    <div>
    </div>
</div>
<div class="container">


<div class="row">
    <div class="col offset-2 col-8">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">id prise de vue</th>
                    <th scope="col">id ecole</th>
                    <th scope="col">date </th>
                    <th scope="col">consulter </th>
                </tr>
            </thead>
            <tbody>
            <?php

                require "C:/xampp/htdocs/Prunelle_florian/config.php";
                $bdd = connect();
                
                
                $sql="select * from prise_de_vue" ; 
                //execution de la requete
                $resultat=$bdd->query($sql);
                    while ($pdv = $resultat->fetch(PDO::FETCH_OBJ)) { ?>
                <tr>
                <td><?= $pdv->Id_prise_de_vue ?></td>
                <td><?= $pdv->id_ecole ?></td>
                <td><?= $pdv->date_pdv?></td>
                <td><a class="btn btn-outline-success" href='show.php?id=<?= $pdv->Id_prise_de_vue ?>'>Consulter </a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
      
    </div>
</div>

<?php
}
else { ?>
<p>page interdite</p>
<a href="/prunelle_florian/accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } ?>


</body>
<script src="/prunelle_florian/script/script.js"></script>

</html>