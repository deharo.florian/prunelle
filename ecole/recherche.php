<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>recherche</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>
<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
<div class="container">
<div class="row text-center py-5">
        <div class="col  alert-warning"><h2>Nos ecoles partenaires</h2></div>
        <div class="row my-4">
             <div class="col col-4 offset-4">
        <?php
   if(isset($_SESSION["action"])){ ?>
            <p class="text-center session alert-warning" >
            <?= $_SESSION["action"]; ?>   </p>  
     <?php  } 
 
 if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
        <a href='formajout.php'  class='btn btn-outline-success' >ajouter</a>
   

     <?php } ?>
     
    
    <div>
  <form class="d-flex my-2" action="recherche.php" method="POST">
    <input class="form-control me-2" type="search" name="barre" placeholder="recherche" aria-label="Search">
    <button class="btn btn-outline-success" type="submit">recherche</button>
    
  </form>
</div> 
 </div>
 </div>
</div>
<div class="row">
<div class="col col-4">
  <a href="ecole.php" class="btn btn-warning m-2">effacer recherche</a>
</div> 

</div>
<div class='container'>


        <div class='row'>
<?php


            // try{
            //     $connect= new PDO('mysql:host=localhost;dbname=bonbons','root','') ;
            // }
            // catch(PDOException $e)
            // {
            //     echo "probleme de connexion à la BDD <br>". $e->getMessage() ;
            // }

            require "C:/xampp/htdocs/Prunelle_florian/config.php";
            $bdd = connect();
            $sql = "select * from ecole where nom like '%$_POST[barre]%' ";
            $resultat = $bdd->query($sql);
            while ($ecole = $resultat->fetch(PDO::FETCH_OBJ)) { ?>
           <div class='card mx-1' style='width: 22rem;'>
        <div class='card-body'>
      <h5 class='card-title'><?= $ecole->nom  ?></h5>
      <p class="card-text">ecole <?= $ecole->genre ?></p>
      <p class="card-text"><?= $ecole->adresse ?></p>
     <p class="card-text"><?= $ecole->ville .", ". $ecole->code_postal?></p>
    <p class="card-text">
        responsable : <?= $ecole->responsable ?> <br> numero de telephone : <?= $ecole->tel ?>
        <br> email : <?= $ecole->email ?>    </p>
        <a href='modifier.php?id=<?= $ecole->Id_ecole ?>' class='btn btn-outline-success' >modifier</a>
     <a href='supprimer.php?id=<?= $ecole->Id_ecole ?>' class='btn btn-outline-success'>supprimer</a>

     
      <p></p>
    </div>
  </div> <?php
            }

          }else{
            ?>
            </div>
    </div>
    <p>page interdite</p>
<a href="accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } ?>
    <?php include("C:/xampp/htdocs/Prunelle_florian/fragment/footer.php") ; ?>      

  </body>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
      <script src="/prunelle_florian/script/script.js"></script>

      </html>


<?php

