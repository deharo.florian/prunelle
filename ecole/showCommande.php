<?php 
session_start() ; 
extract($_GET); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Commande</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>
<style>
      .tableauProduit{
    border :1px solid black ;
        width:100%;
    margin-top:5%;
        text-align: center;
  }

  .tableauPrix{
    border :solid 1px grey ;
    background: #ebebeb;
    margin-top:5%;
    margin-left:70%;
  }

</style>
<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION["autorisation"]) and $_SESSION["autorisation"]=="ok"){ ?>

<div class="container">
    <div class="row">
        <div class="col offset-2 col-8">
           
                <?php
  
                    require "C:/xampp/htdocs/Prunelle_florian/config.php";
                    $bdd = connect();

                    // livraison 

                    $sql = "SELECT * FROM `commande`  inner join utilisateur on utilisateur.Id_utilisateur = commande.id_livreur where Id_commande = $id; ";
                    $res = $bdd->query($sql); 
                    $livreur = $res->fetch(PDO::FETCH_OBJ);
                    
                    ?>
                                        <div class="col "><h2 class="alert-warning text-center"> commande numero <?= $id ?></h2>

                    <?php if($livreur->date_livraison){ ?>
                    <p class="alert-success px-2 my-4 text-left"><b>LIVRAISON :</b><?= $livreur->prenom ." ". $livreur->nom ?> a livré la commande le 
                    <?= dateFR($livreur->date_livraison);}else{ ?>
                        <p class="alert-danger px-2 my-4 text-left"><b>LIVRAISON :</b> la commande  n'a pas encore été livrée </p>
                  <?php  } ?></p>

                  <!-- empochage -->

                        <?php
                  $sql = "SELECT *,TIMEDIFF(date_fin,date_debut) as diff FROM `commande` inner join empoche on commande.Id_commande = empoche.id_commande
                            inner join utilisateur on utilisateur.Id_utilisateur = empoche.id_utilisateur where commande.Id_commande = $id;";
                    $res = $bdd->query($sql); 
                    $empoche = $res->fetch(PDO::FETCH_OBJ);
                    ?> <?php if($empoche->date_empochage){ ?>
                    <p class="alert-success px-2 my-4 text-left"><b>EMPOCHAGE :</b> <?= $empoche->prenom ." ". $empoche->nom ?> a empochée la commande  le 
                    <?= dateHeureFR($empoche->date_empochage);}else{ ?>
                        <p class="alert-danger px-2 my-4 text-left"><b>EMPOCHAGE :</b>   la commande  n'a pas encore été empochée </p>
                  <?php  } ?></p>

                        <?php
                        // empochage -- temps passé sur l'empochage
                        ?> <?php if($empoche->date_debut){ 
                        $sql = "SELECT date_fin,date_debut  FROM `commande` inner join empoche on commande.Id_commande = empoche.id_commande where commande.Id_commande =  $id;";
                $result = $bdd->query($sql); 
                ?>
                <p class="alert-success px-2 my-4 text-left"><b>DATE EMPOCHAGE :</b> <br><?php
                   while($diff = $result->fetch(PDO::FETCH_OBJ)){ 
                           ?>
                        du <?= dateEtHeureFR($diff->date_debut) ." au ". dateEtHeureFR($diff->date_fin)."<br>" ; }?></p>
                       <?php
                   } 
                        // Commande
                    $sql="SELECT * FROM produit INNER join produit_commande on produit.Id_produit = produit_commande.Id_produit where id_commande=$id" ; 
                    //execution de la requete
                    $resultat=$bdd->query($sql);
                    
                    ?>
                    <table  class="table bordered" >
                       <thead >
                        <tr>
                            <td>produit</td>
                            <td>Prix</td>
                            <td>Quantité</td>
                            <td>Reduction</td>
                        </tr>
                       </thead>
                       
                     <?php 
                     
                     $prixHT = 0 ;

                     while ($produit = $resultat->fetch(PDO::FETCH_OBJ)) { ?>
                        <tr>
                            <td scope="col"><?= $produit->nom ?></td>
                            <td scope="col"><?= $produit->prix  ?> €</td>
                            <td scope="col"><?= $produit->quantite  ?></td>
                            <td scope="col"><?= $produit->reduction  ?> €</td>

                        </tr>
                        <?php
                                 $prixHT = $prixHT + $produit->quantite * $produit->prix ;

                    } ?>
                        
                        
                    </table>
                    <table class="tableauPrix">
                        <tr >
                            <td>Total HT </td>
                            <td><?= $prixHT ?> €</td>
                            
                        </tr>
                        <tr >
                            <td>TVA( 0% ) </td>
                            <td><?=  round($prixHT*0)?> €</td>
                            
                        </tr>
                        <tr >
                            <td>Frais de port(fictif) </td>
                            <td>5 €</td>
                            
                        </tr>
                        <tr >
                            <td>TOTAL TTC </td>
                            <td><?= /* round(*/  $prixHT /* *1.196,2)) */ +    5?> €</td>
                            
                        </tr>
                </table>
                    </div>
                </div>
            </div>

<?php
}
else { ?>
<p>page interdite</p>
<a href="/prunelle_florian/accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } ?>


</body>
</html>