<?php 
session_start() ;
extract($_GET);
if(isset($_SESSION["autorisation"]) and $_SESSION["autorisation"]=="ok"){
require "C:/xampp/htdocs/Prunelle_florian/config.php";
$bdd = connect();


$sql="SELECT * FROM `commande` inner join prise_de_vue on commande.id_pdv = prise_de_vue.Id_prise_de_vue inner join ecole on ecole.Id_ecole = prise_de_vue.id_ecole where ecole.Id_ecole = $id; " ; 
//execution de la requete
$resultat=$bdd->query($sql);
$titre = $resultat->fetch(PDO::FETCH_OBJ);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<script src="prunelle_florian/script/script.js"></script>
</head>
<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
    if($titre == true){   ?>
<div class="container">
<div class="row text-center py-5">
        <div class="col alert-warning"><h2>Les commandes de l'ecole <?= $titre->nom ?></h2></div>
        <div class="row my-4">
             <div class="col col-4 offset-4">
         
        <?php
    if(isset($_SESSION["action"])){ ?>
         <p class="text-center session alert-warning" >
       <?= $_SESSION["action"]; ?>   </p>  
 <?php  } ?>  
 
?>
 
   
</div>
<div class="container">


<div class="row">
    <div class="col offset-2 col-8">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">id commande</th>
                    <th scope="col">consulter </th>
                </tr>
            </thead>
            <tbody>
            <?php

                
                // faire count au cas ou il n'y a pas de commandes 
                $sql="SELECT * FROM `commande` inner join prise_de_vue on commande.id_pdv = prise_de_vue.Id_prise_de_vue inner join ecole on ecole.Id_ecole = prise_de_vue.id_ecole where ecole.Id_ecole = $id; " ; 
                //execution de la requete
                $resultat=$bdd->query($sql);

                
                while ($commande = $resultat->fetch(PDO::FETCH_OBJ)) { ?>
                    
                <tr>
                <td><?= $commande->Id_commande ?></td>
                <td><a class="btn btn-outline-success" href='showCommande.php?id=<?= $commande->Id_commande ?>'>Consulter </a></td>
                </tr>
                <?php 

                }?>
                
            </tbody>
        </table>
      
    </div>
</div>

<?php   //cas : 0 commande
            }else{ 
                $sql="SELECT * FROM `ecole`  where ecole.Id_ecole = $id; " ; 
                //execution de la requete
                $resultat=$bdd->query($sql);
                $ecole = $resultat->fetch(PDO::FETCH_OBJ);
                ?>
                
            <div class="container">
                <div class="row text-center py-5">
                    <div class="col "><h2 class="alert-warning">Les commandes de l'ecole <?= $ecole->nom ?></h2>
                <p class="text-center mt-4 h4"><span class=" "> Il n'y a aucune commande !</span> </p><a href="/prunelle_florian/ecole/ecole.php" class="btn btn-warning my-5">Retour</a>
                </div></div>
            </div>
           <?php }
}
// cas : pas autoriser
else { ?>
<p>page interdite</p>
<a href="/prunelle_florian/accueil.php" class="btn btn-danger" role="button">retour</a>
<?php } ?>


</body>
</html>