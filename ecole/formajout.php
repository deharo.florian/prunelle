<?php session_start() ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="\Prunelle_florian\css\style.css">

</head>

<body>
<?php include("C:/xampp/htdocs/Prunelle_florian/fragment/navbar.php") ; 
if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok" ){ ?>
<div class="container">
  <div class="row">
    <div class="col col-6 offset-3">
      <div class="divform">

        <h1 class="text-center my-5">Formulaire d'ajout d'ecole</h1>
        <form action="ajouter.php" method="POST">
          <div class="form-group">
            <label for="nom">entrez le nom</label>
            <input type="text" name="nom" class="form-control" id="nom" placeholder="nom">
        </div>
        <div class="form-group">
          <label for="nom">entrez l'adresse</label>
          <input type="mail" name="adresse" class="form-control" id="nom" placeholder="adresse">
        </div>     
        <div class="form-group">
          <label for="nom">entrez la ville</label>
          <input type="text" name="ville" class="form-control" id="nom" placeholder="ville">
        </div>    
        <div class="form-group">
          <label for="nom">entrez le code postal</label>
          <input type="text" name="code_postal" class="form-control" id="nom" placeholder="code postal">
        </div>    
        <div class="form-group">
          <label for="nom">entrez l'email</label>
          <input type="mail" name="email" class="form-control" id="nom" placeholder="email">
        </div>  
        <div class="form-group">
          <label for="nom">entrez le numero de telephone</label>
          <input type="mail" name="tel" class="form-control" id="nom" placeholder="tel">
        </div>         
        <div class="form-group">
          <label for="nom">entrez la responsable</label>
          <input type="mail" name="responsable" class="form-control" id="nom" placeholder="responsable">
        </div>    
        <div class="form-group">
          <label for="nom">entrez le genre de l'ecole(maternelle,primaire,etc)</label>
          <input type="mail" name="genre" class="form-control" id="nom" placeholder="genre">
        </div>  
        <input type="submit" class="btn btn-success  my-3" value="ajouter">
      </form>
    </div>
    </div>
  </div>
          </div>
          <?php include("C:/xampp/htdocs/Prunelle_florian/fragment/footer.php") ; 
          }else{?> 
          <p>page interdite</p>
          <a href="accueil.php" class="btn btn-danger" role="button">retour</a>
          <?php } ?>     

        </body>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>


</html>