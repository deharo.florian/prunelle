
<nav class="navbar navbar-expand-lg navbar-light bg-secondary">
  <div class="container-fluid">
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">

        <!-- a garder -->
        <?php if(isset($_SESSION['autorisation']) && $_SESSION['autorisation']="ok"){ ?>
                <div class="col mx-1">
                  <a href="/prunelle_florian/logout.php" class="btn alert-secondary">Deconnexion</a>
                </div>
                <div class="col mx-1">
            <a href="/Prunelle_florian/ecole/ecole.php" class="btn alert-warning">Ecole</a>     

            </div>
            <div class="col mx-1">
            <a href="/Prunelle_florian/employe/employe.php" class="btn alert-warning">Employé</a>     

            </div>
            <div class="col mx-1">
            <a href="/Prunelle_florian/pdv/pdv.php" class="btn alert-warning">Prise de vue</a>     

            </div>
            <div class="col mx-1">
            <a href="/Prunelle_florian/Commande/Commande.php" class="btn alert-warning">Commande</a>     

            </div>
         <?php   } else{ ?>
            <div class="col mx-1">
                <a href="/prunelle_florian/admin.php" class="btn alert-warning">Espace admin</a>    
            </div>
        <?php  } ?>
             
            <div class="col mx-1">
            <a href="/prunelle_florian/produit/produit.php" class="btn alert-warning">Produit</a>     

            </div>
            
        
      </ul>

    </div>
  </div>
</nav>